## v0.9.5

- Set multiprocess_files_dir config to temp directory by default
  https://gitlab.com/gitlab-org/prometheus-client-mmap/merge_requests/28
